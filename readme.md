# iPrice Group Code Test [in Node.js] [Typescript]
CLI tool that accepts a string and Operation over string

## Installation

- Basic requirement `Node JS above version 10.x` 

*NPM*

```js
npm install typescript -g;
npm install ts-node -g;
```
```js
npm install
```
Copy @soham directory from `custom_node_modules` to `node_modules`
```js
ts-node src/boot.ts 
```
OR

```js
npm start
```
# Test
For test code 
```js
npm test
```

# Input
in the CLI, application will ask for enter a string, type your string and enter.
```
Please enter your string: soham krishna paul
```

# Output
Output of sample given string will be below:
```
SOHAM KRISHNA PAUL
sOhAm kRiShNa pAuL
CSV created!
```

# CSV File
You will get the CSV file into root directory. named **"data.csv"**
