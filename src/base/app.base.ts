import { Logger } from '@overnightjs/logger';
import { envConfigs, appConfigs } from '../configs';
import { ILogger, IConfig } from '../interfaces';
import { ResponseBuilder } from '../libs';
import {Service} from "typedi";
import * as path from "path";
/**
 *
 * @class AppBase
 */

@Service()
export class AppBase implements ILogger, IConfig {
  public readonly logger: Logger = new Logger();
  public rootPath: string = path.join(__dirname,'..','..');
  public envConfigs: any = envConfigs;
  public appConfigs: any = appConfigs;
  public readonly buildResponse: ResponseBuilder = new ResponseBuilder();
}
