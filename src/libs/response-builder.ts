import { OK } from 'http-status-codes';
import { IResponseBody, IResponseData } from '../interfaces';

export class ResponseBuilder {
  public composeBody(attrs: {
    statusCode?: number,
    status?: string,
    message?: string,
    data?: IResponseData,
  }): IResponseBody {
    // default attributes of response body
    const bodyBase: any = {
      error: false,
      statusCode: OK,
      message: 'Success!',
    };

    // add additional attributes as well as overwrite existing ones with given values
    Object.assign(bodyBase, attrs);

    return bodyBase;
  }
}
