import { BaseModel } from './base.model';
import { HttpStatusCode } from "@soham/config";
import { CustomError } from "../error/customError";
export class SearchDataModel{
  public type!: string;
  public url!:string;
  public keyword!: string;
  public total_count!: any;
  public searched_on!: number;
  constructor(data: any) {
    this.type = data.type || "LANGUAGE";
    this.url = data.url || null;
    this.keyword = data.keyword || null;
    this.total_count = data.total_count || 0;
    this.searched_on = data.searched_on || new Date().getTime();
    this.type = this.type.toUpperCase();
    this.keyword = this.keyword.toLowerCase();
    this.total_count = parseInt(this.total_count);
  }
  public validate() {
    if (!this.keyword) {
      throw new CustomError(
        HttpStatusCode.ValidationError,
        `Keyword is mandatory *`
      );
    }
  }
}
