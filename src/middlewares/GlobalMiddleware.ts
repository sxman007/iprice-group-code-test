import { ExpressMiddlewareInterface, Middleware } from "routing-controllers";
import { json, urlencoded } from "body-parser";
@Middleware({ type: "before" })
export class HeaderCorrection implements ExpressMiddlewareInterface {
  use(request: any, response: any, next: any): any {
    request.headers["Project"] =
      request.headers.Project ||
      request.headers.project ||
      "ALL";
    request.headers["ClientID"] =
      request.headers.ClientID ||
      request.headers.ClientId ||
      request.headers.clientID ||
      request.headers.clientId ||
      request.headers.clientiD ||
      request.headers.clientid ||
      request.headers.DeviceID ||
      request.headers.DeviceId ||
      request.headers.deviceID ||
      request.headers.deviceId ||
      request.headers.deviceiD ||
      request.headers.deviceid ||
      request.headers.Client_ID ||
      request.headers.Client_Id ||
      request.headers.client_ID ||
      request.headers.client_Id ||
      request.headers.client_iD ||
      request.headers.client_id ||
      request.headers.Device_ID ||
      request.headers.Device_Id ||
      request.headers.device_ID ||
      request.headers.device_Id ||
      request.headers.device_iD ||
      request.headers.device_id ||
      null;

    request.headers["PartnerID"] =
      request.headers.PartnerID ||
      request.headers.PartnerId ||
      request.headers.partnerID ||
      request.headers.partnerId ||
      request.headers.partneriD ||
      request.headers.partnerid ||
      request.headers.Partner_ID ||
      request.headers.Partner_Id ||
      request.headers.partner_ID ||
      request.headers.partner_Id ||
      request.headers.partner_iD ||
      request.headers.partner_id ||
      null;

    request.headers["x-access-token"] =
      request.headers["x-access-token"] || request.headers.AuthToken ||
      request.headers.authtoken ||
      request.headers.authToken ||
      request.headers.Auth_Token ||
      request.headers.auth_token ||
      request.headers.auth_Token ||
      request.headers.authorization ||
      request.headers.Authorization ||
      request.headers.Token | request.headers.token ||
      null;
    request.decoded = {};
    next();
  }
}

@Middleware({ type: "before" })
export class JsonBodyParserMiddleware implements ExpressMiddlewareInterface {
  use(request: any, response: any, next: any): any {
    return json({ limit: "50mb" })(request, response, next);
  }
}

@Middleware({ type: "before" })
export class UrlencodedBodyParserMiddleware
  implements ExpressMiddlewareInterface {
  use(request: any, response: any, next: any): any {
    return urlencoded({ limit: "50mb", extended: true })(
      request,
      response,
      next
    );
  }
}
