import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers';
import { Request, Response, NextFunction } from 'express';
import { AppBase } from '../base';

@Middleware({ type: 'before' })
export class AppLogger extends AppBase implements ExpressMiddlewareInterface {
  public use(request: Request, response: Response, next: NextFunction): any {
    this.logger.info(`API: ${request.originalUrl} [${request.method}]`);
    next();
  }
}
