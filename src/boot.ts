import { StringPlay } from './services/StringPlay.service';
import { Container } from 'typedi';
var orig = console.log;
/*
const consoleApply = (argumentsObj:any) => {
    var msgs:any = [];
    while(argumentsObj.length) {
        msgs.push([].shift.call(argumentsObj));
    }
    orig.apply(console, msgs);
    Logger.sendLog(msgs,"CONSOLE");
};
console.log = function() {
    consoleApply(arguments);
};
console.error = function() {
    consoleApply(arguments);
};
*/
process.env.NODE_ENV = process.env.NODE_ENV || "development";
import { CustomLoggerTool as Logger } from "./common/Logger";
import { envConfigs } from "./configs";
import { Application } from 'express';
import { App } from "./app";
Logger.Info(`App running on env: ${process.env.NODE_ENV.toUpperCase()}`);
let serverStarted = false;
let server:any = null;
const StringPlayService = Container.get(StringPlay);
bootstrap().catch(error => console.error(error.stack));
async function startServer(options: any) {
  const { broker } = options;
  let startServer = true;
  if (startServer) {
    const app:Application = new App().getApp();
    server = app.listen(envConfigs.SERVER.PORT, (): void => {
      //Logger.Info(`Server started on port ${envConfigs.SERVER.PORT}.`);
      StringPlayService.run();
    });
    serverStarted = true;
  }
  return server;
}
function stopServer(options: any) {
  const { server, broker } = options;
  if (server && serverStarted) {
    server.close();
    serverStarted=false;
  }
  return null;
}
async function bootstrap() {
  let broker:any = null;
  try{
    server = await startServer({
      broker: broker
    });
  }catch(err){
    console.error(err);
    stopServer({server: server});
    process.exit(1);
  }
}
function exitHandler(options: any, exitCode: any) {
  //if (options.cleanup) console.log("CleanUp", "clean");
  //if (exitCode || exitCode === 0) console.log("Exit Code", exitCode);
  stopServer({server: server});
  process.exit(1);
}
process
  .on("unhandledRejection", (reason: any, p) => {
    switch (reason.name || "") {
      case "MongooseTimeoutError":
        break;
      default:
        console.error(reason);
        console.error(p);
        break;
    }
  })
  .on("uncaughtException", err => {
    console.error("##### Error in process! #####");
    console.error(err);
  })
  .on("exit", exitHandler.bind(null, { cleanup: true }))
  .on("SIGINT", exitHandler.bind(null, { exit: true }))
  .on("SIGUSR1", exitHandler.bind(null, { exit: true }))
  .on("SIGUSR2", exitHandler.bind(null, { exit: true }))
  .on("uncaughtException", exitHandler.bind(null, { exit: true }));
