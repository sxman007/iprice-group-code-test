import { Logger } from '@overnightjs/logger';
export namespace CustomLoggerTool {
        // set default app env
    export function Info(...msg:any){
        sendLog(msg.join(" "),"INFO");
        Logger.Info(msg.join(" "));
    }
    export function info(...msg:any){
        sendLog(msg.join(" "),"INFO");
        Logger.Info(msg.join(" "));
    }
    export function Error(...msg:any){
        sendLog(msg.join(" "),"ERROR");
        Logger.Err(msg.join(" "));
    }
    export function error(...msg:any){
        sendLog(msg.join(" "),"ERROR");
        Logger.Err(msg.join(" "));
    }
    export function Warn(...msg:any){
        sendLog(msg.join(" "),"WARN");
        Logger.Warn(msg.join(" "));
    }
    export function warn(...msg:any){
        sendLog(msg.join(" "),"WARN");
        Logger.Warn(msg.join(" "));
    }
    export function sendLog(content: any,logType:String): void {
        // tslint:disable-next-line:comment-format
        //this.thirdPartyLoggingApplication.doStuff(content);
    }
}