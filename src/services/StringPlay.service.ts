import { Container } from "typedi";
import { StringPlayFileWriterRepo } from "./../repositories/StringPlayFileWriter.repository";
import { BaseService } from "./base.service";
import * as readline from "readline";
const playWriter = Container.get(StringPlayFileWriterRepo);
export class StringPlay extends BaseService {
  public async run() {
    const thisObj = this;
    const readLineObj = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
      terminal: false,
    });
    readLineObj.question("Please enter your string: ", async (string) => {
      try {
        console.log(thisObj.uppercase(string));
        console.log(thisObj.alterUpperLower(string));
        console.log(await playWriter.writeFile(string.split("")));
        readLineObj.close();
      } catch (err) {
        console.error("Something went wrong!!");
        console.error("Close the Application press Ctrl + c");
      }
      console.error("Close the Application press Ctrl + c");
    });
  }
  public uppercase(string: string) {
    return string.toUpperCase();
  }
  public alterUpperLower(string: string) {
    return string
      .split("")
      .map((char, index) =>
        index % 2 == 0 ? char.toLowerCase() : char.toUpperCase()
      )
      .join("");
  }
}
