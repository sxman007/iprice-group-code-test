import { BaseRepository } from "./base.repository";
import { SearchDataModel } from "./../models/SearchData.model";
import * as path from "path";
// tslint:disable-next-line:no-var-requires
const low = require("lowdb");
// tslint:disable-next-line:no-var-requires
const FileSync = require("lowdb/adapters/FileSync");
export class SearchDataRepo extends BaseRepository {
  public getResults(opta: any){
    return new Promise((resolve, reject) => {
      const adapter = new FileSync(
        path.join(this.rootPath, this.envConfigs.DB.PATH)
      );
      const db = low(adapter);
      const returnDataSet: SearchDataModel[] = [];
      let limit = opta.limit || 10;
      let page = opta.page || 1;
      limit = parseInt(limit);
      page = parseInt(page);
      let totalCount = 0;
      let result: any = [];
      if (opta.query) {
        totalCount = db.get("search").filter(opta.query).size().value();
        result = db
          .get("search")
          .filter(opta.query)
          .orderBy(["searched_on"], ["desc"])
          .drop((page - 1) * limit)
          .take(limit)
          .value();
      } else {
        totalCount = db.get("search").size().value();
        result = db
          .get("search")
          .orderBy(["searched_on"], ["desc"])
          .drop((page - 1) * limit)
          .take(limit)
          .value();
      }
      resolve({
        data:
        result.map((eachData: any) => {
          let eachSearchDataModel = new SearchDataModel(eachData);
          eachSearchDataModel.validate();
          return eachSearchDataModel;
        }),
        total_record:totalCount
      });
    });
  }
  public setResult(data: SearchDataModel): Promise<SearchDataModel> {
    return new Promise((resolve, reject) => {
      try {
        const adapter = new FileSync(
          path.join(this.rootPath, this.envConfigs.DB.PATH)
        );
        const db = low(adapter);
        db.defaults({ search: [] }).write();
        db.get("search").push(data).write();
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  }
}
