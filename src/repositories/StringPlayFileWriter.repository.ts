import { BaseRepository } from "./base.repository";
import * as fs from "fs";
import * as path from "path";
import * as os from "os";
export class StringPlayFileWriterRepo extends BaseRepository {
  public async writeFile(data: any) {
    return new Promise((resolve, reject) => {
      try {
        fs.open(
          path.join(this.rootPath, this.envConfigs.DB.PATH),
          "a",
          (err, id) => {
            if (err) {
              return reject(err);
            }
            fs.write(id, data + os.EOL, null, "utf8", (err) => {
              if (err) {
                return reject(err);
              }
              fs.close(id, () => {
                resolve("CSV created!");
              });
            });
          }
        );
      } catch (err) {
        reject(err);
      }
    });
  }
}
