import { StringPlayFileWriterRepo } from './../repositories/StringPlayFileWriter.repository';
import { StringPlay } from './../services/StringPlay.service';
import { Container } from "typedi";
const stringPlayService = Container.get(StringPlay);
const stringPlayRepo = Container.get(StringPlayFileWriterRepo);
describe("search results", () => {
  test("should be equal to", async () => {
    expect(
      await stringPlayService.uppercase("soham")
    ).toBe('SOHAM');
    expect(
      await stringPlayService.alterUpperLower("soham")
    ).toBe('sOhAm');
    expect(
      await stringPlayRepo.writeFile("soham".split(""))
    ).toBe('CSV created!');
  });
});
