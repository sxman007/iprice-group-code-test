import { Logger } from '@overnightjs/logger';

// by applying this decorator route handlers can return response like this -
//
// return 'String PONG';
// return new Promise((resolve, reject) => {resolve('Promise PONG')});
// const obj: any = {a: 1, b: 2};
// res.send(obj.c.d);
// return obj.c.d; // status 500 in response
export const JSONResponse = function(): any {
  return function decorator(t: any, n: any, descriptor: any) {
    const orgFn = descriptor.value;
    if (typeof orgFn === 'function') {
      descriptor.value = async function(...args: any) {
        const responseObject = Array.isArray(args) && args[1]
          ? args[1]
          : {};

        try {
          const result = await orgFn.apply(this, args);
          responseObject.send(result);
        } catch (err) {
          Logger.Err(err, true);
          responseObject.sendStatus(500);
        }
      };
    }
  };
};
