import * as fs from "fs";
import { Logger } from '@overnightjs/logger';
import * as path from "path";
let ENV = process.env.NODE_ENV || 'development';
let envConfig:any;
function getEnvConfig() {
  try {
    ENV=ENV.toLowerCase();
    //const envConfig = require(path.resolve(`${__dirname}/data/${ENV}.json`));
    if(envConfig){
      return envConfig;
    }
    envConfig = JSON.parse(fs.readFileSync(path.resolve(`${__dirname}/data/${ENV}.json`), 'utf-8'));
    return envConfig || {};
  } catch (err) {
    Logger.Err(err);
    return {};
  }
}

export let envConfigs = getEnvConfig();
