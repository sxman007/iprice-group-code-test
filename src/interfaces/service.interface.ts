export interface IServiceGeneric {
  // find a list of resources
  find: (params: any) => any;

  // get a single resource by its id
  get: (id: number|string, params: any) => any;

  // create a new resource
  create: (data: any, params: any) => any;

  // replace an existing resource by its id with data
  update: (id: number|string, data: any, params: any) => any;

  // merge new data into a resource
  patch: (id: number|string, data: any, params: any) => any;

  // remove a resource by its id
  remove: (id: number|string, params: any) => any;
}
