import { IObjectGeneric } from './object.interface';

export interface IResponseData extends IObjectGeneric {
  meta?: any;
  result?: any;
}

export interface IResponseBody {
  message: string,
  _status_code: number,
  _status: string,
  _status_type:string,
  result: any
}
