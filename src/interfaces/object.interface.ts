export interface IObjectGeneric {
  [key: string]: any;
}
